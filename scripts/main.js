function handleFileSelect() {
  var fileSelector = document.getElementById('csv_upload');
  var file = fileSelector.files[0];
  var display = document.getElementById('display');
  if (file) {
    var reader = new FileReader();
    reader.readAsText(file, 'UTF-8');
    reader.onload = function(evt) {
      // parse the csv
      var entries = parse(evt.target.result);

      //convert the date column
      entries = convertDateColumn(entries);

      // transform back into a csv
      var csv = Papa.unparse(entries);

      // download the csv for the user
      download('entries.csv', csv);
    }
    reader.onerror = function(evt) {
      display.innerHTML = 'Error reading entries.csv file';
    }
  }
}

function convertDateColumn(entries) {
  return entries.map(function(row, rowIndex) {
    // Leave the first row alone, it's headers
    if (rowIndex === 0) {
      return row;
    }
    if (rowIndex > 0) {
      // Get the date from the row, convert it, and return a new row with the converted date
      var dateStr = row[2];
      if (dateStr) {
        var convertedDate = convertDate(dateStr);
        row[2] = convertedDate;
      }
      return row;
    }
  });
}

function convertDate(date) {
  var delim = '/';
  if (date.indexOf('-') > 0) {
    delim = '-';
  }
  var [day, month, year] = date.split(delim);
  return year + delim + month + delim + day;
}

// Parse the entries.csv into a data structure where numeric values are converted to numbers from strings
function parse(entries) {
  var result = Papa.parse(entries, {
    dynamicTyping: true
  });
  return result.data;
}

function download(filename, content) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(content));
  element.setAttribute('download', filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}
